﻿using System;
using System.IO;

namespace Logger
{
	internal sealed class FileWriter : IWriter
	{
		private readonly string _filePath;
		private readonly object _syncObj = new object();

		public FileWriter(string filePath)
		{
			if (string.IsNullOrWhiteSpace(filePath))
				throw new ArgumentException(nameof(filePath));

			_filePath = filePath;
		}

		public void WriteMessage(string message)
		{
			if (message == null)
				throw new ArgumentNullException(nameof(message));

			lock (_syncObj)
			{
				using (StreamWriter writer = new StreamWriter(_filePath, true))
				{
					writer.WriteLine(message);
				}
			}
		}
	}
}