﻿using System;

namespace Logger
{
	public sealed class FileLoggerFactory : ILoggerFactory
	{
		private static readonly FileWriterManager FileWriterManager = new FileWriterManager();
		private readonly string _filePath;

		public FileLoggerFactory(string filePath)
		{
			if (string.IsNullOrWhiteSpace(filePath))
				throw new ArgumentException(nameof(filePath));

			_filePath = filePath;
		}

		public ILogger CreateLogger()
		{
			return new Logger(FileWriterManager.GetWriter(_filePath));
		}
	}
}