﻿namespace Logger
{
	public interface ILoggerFactory
	{
		ILogger CreateLogger();
	}
}