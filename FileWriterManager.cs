﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Logger
{
	internal sealed class FileWriterManager
	{
		private readonly Dictionary<string, IWriter> _writers = new Dictionary<string, IWriter>();
		private readonly object _syncObj = new object();

		public IWriter GetWriter(string filePath)
		{
			if (string.IsNullOrWhiteSpace(filePath))
				throw new ArgumentException(nameof(filePath));

			lock (_syncObj)
			{
				IWriter writer;

				if (_writers.ContainsKey(filePath))
				{
					writer = _writers[filePath];
				}
				else
				{
					CheckDirectory(filePath);

					writer = new FileWriter(filePath);
					_writers.Add(filePath, writer);
				}

				return writer;
			}
		}

		private static void CheckDirectory(string filePath)
		{
			var directoryName = Path.GetDirectoryName(filePath);

			if (!Directory.Exists(directoryName))
				Directory.CreateDirectory(directoryName);
		}
	}
}