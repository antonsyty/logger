﻿using System;

namespace Logger
{
	internal sealed class Logger : ILogger
	{
		private readonly IWriter _writer;

		public Logger(IWriter writer)
		{
			if (writer == null)
				throw new ArgumentNullException(nameof(writer));

			_writer = writer;
		}

		private enum LogLevel
		{
			Debug,
			Info,
			Warning,
			Error,
			Fatal
		}

		public void Debug(string message)
		{
			Log(LogLevel.Debug, message);
		}

		public void Debug(string message, Exception exception)
		{
			Log(LogLevel.Debug, message, exception);
		}

		public void DebugFormat(string format, params object[] args)
		{
			Log(LogLevel.Debug, format, args);
		}

		public void Info(string message)
		{
			Log(LogLevel.Info, message);
		}

		public void Info(string message, Exception exception)
		{
			Log(LogLevel.Info, message, exception);
		}

		public void InfoFormat(string format, params object[] args)
		{
			Log(LogLevel.Info, format, args);
		}

		public void Warn(string message)
		{
			Log(LogLevel.Warning, message);
		}

		public void Warn(string message, Exception exception)
		{
			Log(LogLevel.Warning, message, exception);
		}

		public void WarnFormat(string format, params object[] args)
		{
			Log(LogLevel.Warning, format, args);
		}

		public void Error(string message)
		{
			Log(LogLevel.Error, message);
		}

		public void Error(string message, Exception exception)
		{
			Log(LogLevel.Error, message, exception);
		}

		public void ErrorFormat(string format, params object[] args)
		{
			Log(LogLevel.Error, format, args);
		}

		public void Fatal(string message)
		{
			Log(LogLevel.Fatal, message);
		}

		public void Fatal(string message, Exception exception)
		{
			Log(LogLevel.Fatal, message, exception);
		}

		public void FatalFormat(string format, params object[] args)
		{
			Log(LogLevel.Fatal, format, args);
		}

		private void Log(LogLevel level, string message)
		{
			if (message == null)
				throw new ArgumentNullException(nameof(message));

			_writer.WriteMessage($"{DateTime.Now:s} {level} {message}");
		}

		private void Log(LogLevel level, string message, Exception exception)
		{
			if (message == null)
				throw new ArgumentNullException(nameof(message));

			if (exception == null)
				throw new ArgumentNullException(nameof(exception));

			_writer.WriteMessage($"{DateTime.Now:s} {level} {message}: {exception}");
		}

		private void Log(LogLevel level, string format, params object[] args)
		{
			if (format == null)
				throw new ArgumentNullException(nameof(format));

			if (args == null)
				throw new ArgumentNullException(nameof(args));

			_writer.WriteMessage($"{DateTime.Now:s} {level} {string.Format(format, args)}");
		}
	}
}