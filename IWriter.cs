﻿namespace Logger
{
	internal interface IWriter
	{
		void WriteMessage(string message);
	}
}